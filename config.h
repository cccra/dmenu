/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int instant = 1;                 /* return last remaining option immidiately. Switch off with -n */
static int topbar = 1;                  /* -b  option; if 0, dmenu appears at bottom     */
static int fuzzy = 1;                   /* -F  option; if 0, dmenu doesn't use fuzzy matching     */
static int centered = 0;                /* -c option; centers dmenu on screen */
static int min_width = 600;             /* minimum width when centered */
static unsigned int lines = 0;          /* -l and -g options; controls number of lines and columns in grid if > 0 */
static unsigned int columns    = 0;
static unsigned int lineheight = 0;     /* -h option; minimum height of a menu line     */
static const char *prompt = NULL;       /* -p  option; prompt to the left of input field */
static unsigned int border_width = 0;   /* Size of the window border */

/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = { "monospace:size=8:style=bold", "JoyPixels:pixelsize=10:antialias=true:autohint=true" };

static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#fbf1c7", "#131111" },
	[SchemeSel] = { "#fbf1c7", "#353333" },
	[SchemeSelHighlight] = { "#ffb64d", "#353333" },
	[SchemeNormHighlight] = { "#e7803d", "#131111" },
	[SchemeOut] = { "#000000", "#00ffff" },
};

/* static const unsigned int bgalpha = 0xe0; */
static const unsigned int bgalpha = OPAQUE;
static const unsigned int fgalpha = OPAQUE;

static const unsigned int alphas[SchemeLast][2] = {
	/*		fgalpha		bgalphga	*/
	[SchemeNorm] = { fgalpha, bgalpha },
	[SchemeSel] = { fgalpha, bgalpha },
	[SchemeSelHighlight] = { fgalpha, bgalpha },
	[SchemeNormHighlight] = { fgalpha, bgalpha },
	[SchemeOut] = { fgalpha, bgalpha },
};

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = "";
